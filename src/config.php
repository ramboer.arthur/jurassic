<?php

$loader = new \Twig\Loader\FilesystemLoader (dirname(dirname(__FILE__)) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', '\Twig\Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new \Twig\Extension\DebugExtension()); // Add the debug extension
    $twig->addGlobal('ma_valeur', "Hello There!");

   // $twig->addFilter(new \Twig\Twig_Filter('markdown', function($string){
   //     return Markdown::defaultTransform($string);
   // }));
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});